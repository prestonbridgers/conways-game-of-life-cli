#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

#define MICRO2MILLI 1000
#define ALIVE_CHAR "X"
#define DEAD_CHAR " "

/*
####################################################
#
#                 Begin Cell Code
#
####################################################
*/

typedef struct
{
	int x;
	int y;
	bool isAlive;
} Cell;

Cell *Cell_create(int x, int y)
{
	Cell *c_new = calloc(1, sizeof(Cell));
	// DEBUG
	bool alive = random() % 2;
	*c_new = (Cell) {x, y, alive};
	return c_new;
}

int Cell_destroy(Cell *cell)
{
	if (cell != NULL)
	{
		free(cell);
		return 0;
	}
	else
	{
		return 1;
	}
}

int Cell_flip(Cell *cell)
{
	if (cell == NULL)
		return 1;

	if (cell->isAlive)
		cell->isAlive = false;
	else
		cell->isAlive = true;
	
	return 0;
}

int Cell_print(Cell *cell)
{
	if (cell == NULL)
		return 1;
	
	printf("Cell: (%d, %d, %d)\n", cell->y, cell->x, cell->isAlive);
	return 0;
}

/*
####################################################
#
#                 Begin Table Code
#
####################################################
*/

typedef struct
{
	int width;
	int height;
	Cell **cells;
} Table;

Table *Table_create(int width, int height)
{
	Table *t_new = calloc(1, sizeof(Table));
	Cell **cells = calloc(width * height, sizeof(Cell*));

	int index;
	for (int row = 0; row < height; row++)
	{
		for (int col = 0; col < width; col++)
		{
			index = row * width + col;
			cells[index] = Cell_create(col, row);
		}
	}

	*t_new = (Table) {width, height, cells};
}

int Table_destroy(Table *t)
{
	if (t == NULL)
		return 1;

	int index;
	for (int row = 0; row < t->height; row++)
	{
		for (int col = 0; col < t->width; col++)
		{
			index = row * t->width + col;
			free(t->cells[index]);
		}
	}
	free (t->cells);
	free(t);

	return 0;
}

int Table_print(Table *t)
{
	if (t == NULL)
		return 1;

	printf("Printing Table:\n");
	int index;
	for (int i = 0; i < t->height; i++)
	{
		for (int j = 0; j < t->width; j++)
		{
			index = i * t->width + j;
			Cell_print(t->cells[index]);
		}
	}
	printf("End Printing Table\n");
	
	return 0;
}

int Table_draw(Table *t)
{
	if (t == NULL)
		return 1;

	for (int i = 0; i < 50; i++)
		printf("\n");

	int index;
	for (int row = 0; row < t->height; row++)
	{
		for (int col = 0; col < t->width; col++)
		{
			index = row * t->width + col;
			if (t->cells[index]->isAlive)
				printf(ALIVE_CHAR);
			else
				printf(DEAD_CHAR);
		}
		printf("\n");
	}

	return 0;
}

int Table_countAdjLivingCells(Table *t, int index)
{
	int liveCellCount = 0;
	int width = t->width;
	int height = t->height;

	int x = index % width;
	int y = (index - x) / width;

	if (x == 0 || x == width - 1 ||
	    y == 0 || y == height - 1)
	{
		return 0;
	}

	Cell *cellNW = t->cells[(index - width) - 1];
	Cell *cellN = t->cells[(index - width)];
	Cell *cellNE = t->cells[(index - width) + 1];
	Cell *cellW = t->cells[index - 1];
	Cell *cellE = t->cells[index + 1];
	Cell *cellSW = t->cells[(index + width) - 1];
	Cell *cellS = t->cells[(index + width)];
	Cell *cellSE = t->cells[(index + width) + 1];
		
	//TODO: Fix edge cases
	liveCellCount += cellNW->isAlive;
	liveCellCount += cellN->isAlive;
	liveCellCount += cellNE->isAlive;
	liveCellCount += cellW->isAlive;
	liveCellCount += cellE->isAlive;
	liveCellCount += cellSW->isAlive;
	liveCellCount += cellS->isAlive;
	liveCellCount += cellSE->isAlive;

	return liveCellCount;
}	

int Table_update(Table *t)
{
	if (t == NULL)
		return 1;

	bool doFlip[t->width * t->height];
	for (int i = 0; i < t->width * t->height; i++)
		doFlip[i] = false;

	int index;
	for (int row = 0; row < t->height; row++)
	{
		for (int col = 0; col < t->width; col++)
		{
			index = row * t->width + col;
			Cell *current_cell = t->cells[index];
			int livingNeighbors = Table_countAdjLivingCells(t, index);

			if (current_cell->isAlive)
			{
				if (livingNeighbors < 2 ||
				    livingNeighbors > 3)
					doFlip[index] = true;
			}
			else // current_cell is dead
			{
				if (livingNeighbors == 3)
					doFlip[index] = true;
			}
		}
	}

	for (int row = 0; row < t->height; row++)
	{
		for (int col = 0; col < t->width; col++)
		{
			index = row * t->width + col;
			Cell *current_cell = t->cells[index];

			if (doFlip[index])
				Cell_flip(current_cell);
		}
	}

	return 0;
}

/*
####################################################
#
#                 Begin Main Code
#
####################################################
*/

int main(int argc, char** argv)
{
	srandom(time(0));

	Table *t = Table_create(175, 55);

	for (;;)
	{
		Table_draw(t);
		Table_update(t);
		usleep(150 * MICRO2MILLI);
	}

	Table_destroy(t);

	return 0;
}
